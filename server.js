const Koa = require('koa')
const app = new Koa()
const KoaStatic = require('koa-static')
const path = require('path')

const port = 8080

app.use(KoaStatic(path.join(__dirname, './build')))


app.listen(port, () => {
    console.log('服务器启动成功', port)
})

