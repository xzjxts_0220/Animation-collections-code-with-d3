/*
 * @Author: px chen reruolforpx@gmail.com
 * @Date: 2023-10-31 20:23:03
 * @LastEditors: px chen reruolforpx@gmail.com
 * @LastEditTime: 2023-10-31 20:27:31
 * @FilePath: /Animation-collections-code-with-d3/src/main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import 'babel-polyfill';
// import  "react-app-polyfill/ie11";
// import  "react-app-polufill/stable";
import { inject } from '@vercel/analytics';
import React from 'react';
import ReactDOM from 'react-dom';
import RouterComponent from './router';
import './assets/public_css/index.css';
import * as serviceWorker from './serviceWorker';

inject()
class Index extends React.Component{
  render(){
    return(
        <React.Fragment>
            <RouterComponent />
        </React.Fragment>
    )
}
}

ReactDOM.render(<Index />,document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
