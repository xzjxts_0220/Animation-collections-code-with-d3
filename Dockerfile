FROM node:16.16.0

WORKDIR /usr/src/app

COPY . .

RUN yarn --only=prod --registry=https://registry.npm.taobao.org

RUN yarn build

EXPOSE 8080

ENV PORT 8080

CMD ["node","server.js"]



